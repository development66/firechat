import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  isLoading: boolean;
  mensaje: string = '';
  elemento: any;

  constructor(public chatService: ChatService) {
    this.chatService.cargarMensajes()
    .subscribe(() => {
      timer(20).subscribe(() => this.elemento.scrollTop = this.elemento.scrollHeight)
    });
  }

  ngOnInit() {
    this.elemento = document.querySelector('#app-mensajes');
  }

  enviarMensaje() {
    if(this.mensaje.length === 0 || this.mensaje.trim() === '') {
      this.mensaje = this.mensaje.trim();
      return;
    }
    console.log('mensaje ', this.mensaje , this.mensaje.trim());
    this.isLoading = true;
    this.chatService
      .agregarMensaje(this.mensaje.trim())
      .then(() => {
        this.isLoading = false;
        this.mensaje = "";
      })
      .catch((err) => {
        console.log(`error en mensaje ${err}`)
        this.isLoading = false;
      });
  }

}
