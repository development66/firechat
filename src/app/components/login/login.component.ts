import { Component } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {

  constructor(private chatService: ChatService) { }

  ingresar(proveedor: string) {
    this.chatService.login(proveedor);
  }

}
