import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Mensaje } from '../interface/mensaje.interface';
import { Usuario } from '../interface/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;

  public chats: Mensaje[] = [];
  public usuario: Usuario = {};

  constructor(
    private afs: AngularFirestore,
    public auth: AngularFireAuth
  ) {
    this.auth.authState.subscribe(user => {
      console.log(`Estado del usuario`, user);
      
      if(!user) {
        return;
      }

      this.usuario.uid = user.uid;
      this.usuario.nombre = user.displayName;
      this.usuario.img = user.photoURL;
    })
  }

  login(proveedor: string) {
    proveedor === 'google' ?
    this.auth.signInWithPopup(new auth.GoogleAuthProvider()) :
    this.auth.signInWithPopup(new auth.TwitterAuthProvider());
  }

  logout() {
    this.usuario = {};
    this.auth.signOut();
  }

  cargarMensajes(): Observable<Mensaje[]> {
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'desc').limit(5));

    return this.itemsCollection.valueChanges()
    .pipe(map((mensajes: Mensaje[]) => {
      return this.chats = mensajes.sort((a, b) => a.fecha - b.fecha);
    }));
  }

  agregarMensaje(texto: string) {
    let mensaje: Mensaje = {
      uid: this.usuario.uid,
      nombre: this.usuario.nombre,
      mensaje: texto,
      fecha: new Date().getTime()
    }
    return this.itemsCollection.add(mensaje);
  }
}
